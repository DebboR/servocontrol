/*
 * MotorControl.h
 *
 * Created: 28/07/2014 0:00:22
 *  Author: robbe_000
 */ 


#ifndef MOTORCONTROL_H_
#define MOTORCONTROL_H_

#include <asf.h>
#include <tc45.h>
#include <ioport.h>
#include "Constants.h"

#define KP 0.04
#define KI 0.002
#define KD 0.0009

void mcSetSpeedPol(char speed, char dir);
void mcSetSpeedUnpol(char speed);
void mcSetPosition(int p);
void mcPIDControl();

#endif /* MOTORCONTROL_H_ */