/**
 * \file
 *
 * \brief User board initialization template
 *
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include "Constants.h"

void board_init(void)
{
	/* This function is meant to contain board-specific initialization code
	 * for, e.g., the I/O pins. The initialization can rely on application-
	 * specific board configuration, found in conf_board.h.
	 */
	ioport_set_pin_dir(MOTOR, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(DIR, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(BRAKE, IOPORT_DIR_OUTPUT);
	
	//Init interrupts
	sei();
	
	//Init MotorControl PWM
	tc45_enable(&TCC5);
	tc45_set_wgm(&TCC5, TC45_WG_SS);
	tc45_write_period(&TCC5, 200);
	tc45_enable_cc_channels(&TCC5, TC45_CCACOMP);
	tc45_write_clock_source(&TCC5, TC45_CLKSEL_DIV1_gc);
	
	//Init MotorControl QDEC
	qdec_get_config_defaults(&qConf);
	qdec_config_phase_pins(&qConf, &PORTD, 0, false, 10);
	qdec_config_revolution(&qConf, 65535);
	qdec_config_disable_index_pin(&qConf);
	qdec_config_tc(&qConf, &TCC4);
	qdec_enabled(&qConf);
	
	//Init PID Timer
	
}
