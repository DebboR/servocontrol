/*
 * MotorControl.c
 *
 * Created: 27/07/2014 23:55:36
 *  Author: robbe_000
 */ 

#include "MotorControl.h"

int mcP, mcI, mcD;
int mcActualPos;
int mcDifference;
int mcWantedPos = 0;
int mcError = 0;
int mcLastDifference = 0;

void mcSetSpeedUnpol(char speed){
	if(speed < 0){
		mcSetSpeedPol(-1*speed, 0);
	} else {
		mcSetSpeedPol(speed, 1);
	}
}

void mcSetSpeedPol(char speed, char dir){
	if(speed < 0)
		speed = 0;
	if(speed > 200)
		speed = 200;
	
	tc45_write_cc(&TCC5, TC45_CCA, (2*speed));
	
	if(dir != 0)
		ioport_set_pin_level(DIR, true);
	else
		ioport_set_pin_level(DIR, false);
		
	if(speed > 0)
		ioport_set_pin_level(BRAKE, false);
	else
		ioport_set_pin_level(BRAKE, true);
}

void mcSetPosition(int p){
	mcWantedPos = p;
}


void mcPIDControl(){
	mcActualPos = qdec_get_position(&qConf);
	mcDifference = mcWantedPos - mcActualPos;
	mcP = mcDifference * KP;
	mcError += mcDifference;
	mcI = mcError * KI;
	mcD = (mcLastDifference-mcDifference) * KD;
	int res = mcP + mcI + mcD;
	if(res>100)
		res = 100;
	else if(res<-100)
		res = -100;
	mcSetSpeedUnpol(-1*res);
	mcLastDifference = mcDifference;
}