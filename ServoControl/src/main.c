#include <asf.h>
#include <tc45.h>

#define F_CPU 32000000
#include <util/delay.h>
#include "MotorControl.h"

int main (void)
{
	sysclk_init();
	board_init();
	
	char d = 0;
	mcSetPosition(5000);
	int i=0;
	while(1){
		mcActualPos = qdec_get_position(&qConf);
		mcPIDControl();
		_delay_ms(10);
		mcSetPosition(50*i);
		if(i == 1000){
			i = 0;
		}
		i++;
	}
}
