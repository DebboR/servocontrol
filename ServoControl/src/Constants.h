/*
 * Constants.h
 *
 * Created: 28/07/2014 0:02:27
 *  Author: robbe_000
 */ 


#ifndef CONSTANTS_H_
#define CONSTANTS_H_

qdec_config_t qConf;

#define MOTOR IOPORT_CREATE_PIN(PORTC, 4)
#define DIR IOPORT_CREATE_PIN(PORTA, 0)
#define BRAKE IOPORT_CREATE_PIN(PORTA, 1)

#endif /* CONSTANTS_H_ */